
$('.ui.dropdown')
  .dropdown()
;

$('.ui.form')
.form({
	candidateId: {
	    identifier: 'candidateId',
	    rules: [
	      {
	        type: 'empty',
	        prompt: 'Please select a candidate'
	      }
	    ]
	  },
	  office: {
		    identifier: 'office',
		    rules: [
		      {
		        type: 'empty',
		        prompt: 'Please select an office'
		      }
		    ]
		  },
  amountDonated: {
    identifier: 'amountDonated',
    rules: [
      {
        type: 'empty',
        prompt: 'Please select an amount to donate'
      }
    ]
  }
})
;
