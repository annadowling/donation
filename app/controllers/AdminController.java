package controllers;

import java.util.List;

import models.*;
import play.*;
import play.mvc.*;

public class AdminController extends Controller {
   
	public static void index()
    {
		List<User> users = User.findAll();
    	List<Candidate> candidates = Candidate.findAll();
		render(users, candidates);
    }
	
	 public static void adminLogin()
	  {
	    render();
	    
	  }
	 
	 public static void logout()
	  {
	    session.clear();
	    Welcome.index();
	  }
	 
	 public static void addCandidate()
	 {
		 render();
	 }
	 
	 public static void register(String firstName, String lastName, String office)
		{
			Logger.info(firstName + " " + lastName + " " + office + " ");

			Candidate candidate = new Candidate (firstName, lastName, office, 0);
			candidate.save();
			session.put("logged_in_candidateid", candidate.id);
			 AdminController.addCandidate();

		}
	
	public static void authenticateAdmin(String userName, String password)
	  {
	    Logger.info("Attempting to authenticate with " + userName + ":" +  password);

	    Admin admin = Admin.findByuserName(userName);
	    if ((admin != null) && (admin.checkPassword(password) == true))
	    {
	      Logger.info("Successful authentication of " + admin.userName);
	      session.put("logged_in_userid", admin.id);
	      AdminController.index();
	    }
	    else
	    {
	      Logger.info("Authentication failed");
	      adminLogin();  
	    }
	  }
	
	public static Admin getCurrentAdmin() 
    {
        String adminId = session.get("logged_in_adminid");
        if(adminId == null)
        {
            return null;
        }
        Admin logged_in_admin = Admin.findById(Long.parseLong(adminId));
        Logger.info("In Accounts controller: Logged in admin is " + logged_in_admin.userName);
        return logged_in_admin;
    }
  
	
	
}