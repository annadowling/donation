package controllers;

import play.*;
import play.mvc.*;
import java.util.*;
import models.*;

public class Accounts extends Controller
{
	
	 public static void signup()
	  {
	    render();
	  }

	  public static void login()
	  {
	    render();
	    
	  }
	  
	  public static void EditDetails()
	  {
	    render();
	  }
	  
	  public static void logout()
	  {
	    session.clear();
	    Welcome.index();
	  }

	 

	  public static void index()
	  {
		 
	    render();
	  }
	  
	  
	  
	  public static void register(boolean usaCitizen, String firstName, String lastName, String email, String password, int age, String addressLine1, String addressLine2, String townCity, String state, String postCode, String country)
		{
			Logger.info(usaCitizen + " " + firstName + " " + lastName + " " + email + " " + password + " " + age + " " + addressLine1 + " " + addressLine2 + " " + townCity + " " + state + " " + postCode + " " + country + " ");

			User user = new User (usaCitizen, firstName, lastName, email, password, age, addressLine1, addressLine2, townCity, state, postCode, country);
			user.save();
			session.put("logged_in_userid", user.id);
			DonationController.index();

		}



	  public static void authenticate(String email, String password)
	  {
	    Logger.info("Attempting to authenticate with " + email + ":" +  password);

	    User user = User.findByEmail(email);
	    if ((user != null) && (user.checkPassword(password) == true))
	    {
	      Logger.info("Successful authentication of " + user.firstName + " " + user.lastName);
	      session.put("logged_in_userid", user.id);
	      DonationController.index();
	    }
	    else
	    {
	      Logger.info("Authentication failed");
	      login();  
	    }
	  }
	  
	  public static User getCurrentUser() 
	    {
	        String userId = session.get("logged_in_userid");
	        if(userId == null)
	        {
	            return null;
	        }
	        User logged_in_user = User.findById(Long.parseLong(userId));
	        Logger.info("In Accounts controller: Logged in user is " + logged_in_user.firstName);
	        return logged_in_user;
	    }
	  
	  public static void changeName(String firstName, String lastName)
		{
			String userId = session.get("logged_in_userid");
			User user = User.findById(Long.parseLong(userId));
			user.firstName = firstName;
			user.lastName = lastName;
			user.save();
			Logger.info("Name changed to " + firstName + lastName);
			EditDetails();
		} 
	  
	  public static void changeEmail(String email)
		{
			String userId = session.get("logged_in_userid");
			User user = User.findById(Long.parseLong(userId));
			user.email = email;
			user.save();
			Logger.info("Email changed to " + email);
			EditDetails();
		}
	  
	  public static void changePassword(String password)
		{
			String userId = session.get("logged_in_userid");
			User user = User.findById(Long.parseLong(userId));
			user.password = password;
			user.save();
			Logger.info("Password changed to " + password);
			EditDetails();
		}
	  
	  public static void changeAge(int age)
		{
			String userId = session.get("logged_in_userid");
			User user = User.findById(Long.parseLong(userId));
			user.age = age;
			user.save();
			Logger.info("Age changed to " + age);
			EditDetails();
		}
	  
	  public static void changeLocation(String addressLine1, String addressLine2, String townCity, String state, String postCode, String country)
		{
			String userId = session.get("logged_in_userid");
			User user = User.findById(Long.parseLong(userId));
			user.addressLine1 = addressLine1;
			user.addressLine2 = addressLine2;
			user.townCity = townCity;
			user.state = state;
			user.postCode = postCode;
			user.country = country;
			user.save();
			Logger.info("Location changed to " + addressLine1 + addressLine2 + townCity + state + postCode + country);
			EditDetails();
		}
}
	  
	  
	  
	 


