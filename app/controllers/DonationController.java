package controllers;

import play.*;
import play.mvc.*;
import java.util.*;
import models.*;
import javax.persistence.Query;
import play.db.jpa.JPA;
import play.db.jpa.Model;


public class DonationController extends Controller

{
	
	public static void index()
	{
		User user = Accounts.getCurrentUser();
		List<Candidate> candidates = Candidate.findAll();
		if (user == null)
		{
			Logger.info ("Donation class : Unable to getCurrentUser");
			Accounts.login();
		}
		else
		{
			getPercentTargetAchieved();
			Logger.info("Donation ctrler : user is " + user.email);
			Logger.info("Donation ctrler : percent target achieved");
			render(user, candidates);
		}
				
	}
	
	public static void donate(String candidateId, long amountDonated, String methodDonated)
	{
		Logger.info("candidate id" +  candidateId + "" + "amount donated" +  amountDonated + "" + "method donated" +  methodDonated);
		Candidate target = Candidate.findById(Long.parseLong(candidateId));
		User user = Accounts.getCurrentUser();
		if (user == null)
		{
			Logger.info ("Donation class : Unable to getCurrentUser");
			Accounts.login();
		}
		else
		{
			addDonation(user, target, amountDonated, methodDonated);
		}
		index();
	}
	
	private static void addDonation(User user, Candidate candidate, long amountDonated, String methodDonated)
	{
		Donation bal = new Donation(user, candidate, amountDonated, methodDonated);
		bal.save();
		
	}
	
	private static void getPercentTargetAchieved()
	{
		List<Candidate> candidates = Candidate.findAll();
		for(Candidate candidate : candidates)
		{
			double progress = 0;
			for(int i=0; i<candidate.donations.size(); i++)
			{
				long a = candidate.donations.get(i).received;
				progress = progress += a;
				
			}
			progress = progress/200;
			candidate.progress = progress;
			candidate.save();
		
		}
		
	}
	
	public static void renderReport()
	{
		List<Donation> donationFilter = Donation.findAll();
		List<String> countries = Arrays.asList("America", "Britain", "Canada", "Ireland", "Russia");
		List<Candidate> candidates = Candidate.findAll();
		render(donationFilter, countries, candidates);
	}
	
	public static void byAge(int to, int from)
	{  
		Logger.info("Filter "+ from +" - "+ to);
		List<Donation> donations = Donation.findAll();
		List<Donation> donationFilter = new ArrayList<Donation>();
		List<String> countries = Arrays.asList("America", "Britain", "Canada", "Ireland", "Russia");
		List<Candidate> candidates = Candidate.findAll();
		
		Logger.info("Donation "+ donations.size());
		for(int i=0; i < donations.size(); i++)
		{
			Logger.info("This donation "+ donations.get(i).from.age);
			
		if((donations.get(i).from.age >= from) && (donations.get(i).from.age <= to))
		{
			donationFilter.add(donations.get(i));
		}
		
		}
		render("DonationController/renderReport.html", donationFilter, countries, candidates);
		
		}
	
	public static void byCountry(String country)
	{  
		List<Donation> donations = Donation.findAll();
		List<Donation> donationFilter = new ArrayList<Donation>();
		List<String> countries = Arrays.asList("America", "Britain", "Canada", "Ireland", "Russia");
		List<Candidate> candidates = Candidate.findAll();
		
		
		for (int i=0; i < donations.size(); i++)

		{
			if(donations.get(i).from.country.equals(country))
			{ 
				donationFilter.add(donations.get(i));

			}
		
		
		}
		render("DonationController/renderReport.html", donationFilter, countries, candidates);
		
		}
	
	public static void byCandidate(String byCandidate)
	{  
		List<Donation> donations = Donation.findAll();
		List<Donation> donationFilter = new ArrayList<Donation>();
		List<String> countries = Arrays.asList("America", "Britain", "Canada", "Ireland", "Russia");
		List<Candidate> candidates = Candidate.findAll();
		Candidate target = Candidate.findById(Long.parseLong(byCandidate));
		
		for (int i=0; i < donations.size(); i++)

		{
			if(donations.get(i).to == target)
			{ 
				donationFilter.add(donations.get(i));

			}
		
		
		}
		render("DonationController/renderReport.html", donationFilter, countries, candidates);
		
		}
	
	

	  
	

}
