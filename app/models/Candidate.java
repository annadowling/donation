package models;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import play.db.jpa.Model;

@Entity
public class Candidate extends Model 
{
	public String firstName;
	public String lastName;
	public String office;
	public double progress;
	
	@OneToMany(mappedBy = "to")
	 public List<Donation> donations;
	
  
	public Candidate(String firstName, String lastName, String office, double progress)
	{
		this.firstName = firstName;
		this.lastName = lastName;
		this.office = office;
		this.progress = progress;
	}
	
	
}