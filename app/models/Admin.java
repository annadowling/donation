package models;

import play.db.jpa.Model;
import models.*;
import play.*;
import play.mvc.*;

import javax.persistence.Entity;
import javax.persistence.Query;

@Entity
public class Admin extends Model
{
 
	public String userName;
	public String password;
	
	public Admin(String userName, String password)
	{
		this.userName = userName;
		this.password = password;
	}
	
	public static Admin findByuserName(String userName) 
    {
        return find("userName", userName).first();
    }

    public boolean checkPassword(String password) 
    {
        return this.password.equals(password);
    }
}
