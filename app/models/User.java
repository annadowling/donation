package models;

  import java.util.ArrayList;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import java.util.ArrayList;
import java.util.List;

import play.db.jpa.Model;

  @Entity
  public class User extends Model
  {
    public boolean usaCitizen;
    public String firstName;
    public String lastName;
    public String email;
    public String password;
    public int age;
    public String addressLine1;
    public String addressLine2;
    public String townCity;
    public String state;
    public String postCode;
    public String country;
    

    public User
    (         
    		  boolean usaCitizen,
              String firstName, 
              String lastName, 
              String email, 
              String password,
              int age,
              String addressLine1,
    		  String addressLine2,
    		  String townCity,
    		  String state,
    		  String postCode,
    		  String country
              )
    {
      this.usaCitizen = usaCitizen;
      this.firstName = firstName;
      this.lastName = lastName;
      this.email = email;
      this.password = password;
      this.age = age;
      this.addressLine1 = addressLine1;
      this.addressLine2 = addressLine2;
      this.townCity = townCity;
      this.state = state;
      this.postCode = postCode;
      this.country = country;
    }

    public static User findByEmail(String email) 
    {
        return find("email", email).first();
    }

    public boolean checkPassword(String password) 
    {
        return this.password.equals(password);
    }
  }

