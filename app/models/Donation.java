package models;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import play.db.jpa.Model;

@Entity
public class Donation extends Model 
{
	 public long received;
	 public String methodDonated;
	 
  @ManyToOne
   public User from;
  
  @ManyToOne
   public Candidate to;
  
	public Donation(User source, Candidate target, long received, String methodDonated)
	{
		from = source;
		to = target;
		this.received = received;
		this.methodDonated = methodDonated;
	}

}

